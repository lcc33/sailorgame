require 'gosu'

# Tutorial window
class Tutorial < Gosu::Window
  def initialize(width: 640, height: 480)
    super width, height
    self.caption = 'Tutorial'
    @j1 = Joint.new(320, 240) # initiates a "Joint"
    @j1.accelerate(1, Math::PI/4) # and accelerates it one time
  end

  def update
    @j1.update # update joint
  end

  def draw
    @j1.draw # draw joint
  end
end

# Joint
class Joint
  def initialize(x, y)
    @x_position = x # store the horizontal
    @y_position = y # and vertical positions
    @z_position = 0 # (not worried about z just yet)

    @velocity = 0 # velocity stored in polar magnitude
    @direction = 0 # and angle

    @image = Gosu::Image.new('pirate_doodle.png') # just a png of a circle
    @waves = Gosu::Image.new('waves2.png')
    @font = Gosu::Font.new(20) # for debugging
    @iteration = 0
    @side = 0
  end

  def accelerate(magnitude, direction)
    delta_h_velocity = magnitude * Math.cos(direction) # acceleration applies a horizontal
    delta_v_velocity = magnitude * Math.sin(direction) # and vertical change in velocity

    h_velocity = @velocity * Math.cos(@direction) + delta_h_velocity
    v_velocity = @velocity * Math.sin(@direction) + delta_v_velocity
    
    @velocity = Math.sqrt(h_velocity*h_velocity + v_velocity*v_velocity)
    @direction = Math.atan2(v_velocity, h_velocity)
  end

  def bounce
    @direction = Math::PI/2+@direction
  end

  # apply velocity to the position
  def update
    apply_drag
    
    h_velocity = @velocity * Math.cos(@direction)
    v_velocity = @velocity * Math.sin(@direction)
    @x_position += h_velocity
    @y_position += v_velocity
    bounce if out_of_bounds?

    update_input
    
  end

  def apply_drag
    @velocity += -@velocity*0.005
  end

  def update_input
    if Gosu.button_down? Gosu::KbDown
      if (@direction%(2*Math::PI))<Math::PI/2 || (@direction%(2*Math::PI))>3*Math::PI/2
        @direction+= 0.1
      else
        @direction+= -0.1
      end
    end
    if Gosu.button_down? Gosu::KbUp
      if (@direction%(2*Math::PI))<Math::PI/2 || (@direction%(2*Math::PI))>3*Math::PI/2
        @direction+= -0.1
      else
        @direction+= 0.1
      end
    end
    if Gosu.button_down? Gosu::KbLeft
      @direction+= -0.1
    end
    if Gosu.button_down? Gosu::KbRight
      @direction+= 0.1
    end
    if Gosu.button_down? Gosu::KbSpace
      @velocity += @velocity*0.01
    end
  end

  def draw
    jj=0
    @iteration+=1
    if (@iteration==50)
      @side+=1
      @iteration = 0
    else
    end

    loop do
      if jj%2==0 && @side%2==0
        offset = 10
      else
        offset = 0
      end
      ii=0
      loop do
        
        @waves.draw(
          ii*60-offset,
          jj*50, 
          0,
          0.05, 0.05
        )
        ii +=1
        if ii==11
          break
        end
      end
      if jj==9
        break
      end
      jj+=1
    end

    @image.draw_rot(
      @x_position,
      @y_position,
      @z_position,
      @direction*180/Math::PI+180,
      0.5, 0.5,
      0.05, 0.05
    )

    @font.draw_text("velocity: #{@velocity}\ndirection: #{@direction}", 0, 0, 0)
  end

  private

    def out_of_bounds?
      @x_position <= 0 || @x_position >= (640-20) || @y_position <= 0 || @y_position >= (480-20)
    end
end

tut = Tutorial.new
tut.show