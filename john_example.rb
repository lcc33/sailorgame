require 'gosu'
 
# Tutorial window
class Tutorial < Gosu::Window
  def initialize(width: 640, height: 480)
    super width, height
    self.caption = 'Tutorial'
    @player = Joint.new(320, 240) # initiates player
    @input = InputControl.new(@player)
  end
 
  def update
    dt = delta_time
    @input.update(dt)
    @player.update(dt) # update player
  end
 
  def draw
    @player.draw # draw player
  end
 
  private
 
    def delta_time
      @last_elapsed ||= Gosu::milliseconds()
      elapsed = Gosu::milliseconds()
      dt = elapsed - @last_elapsed
      @last_elapsed = elapsed
      dt = 0 if dt < 0 # Gosu::milliseconds() doesn't handle max numeric values so eventually just starts back at zero
      dt
    end
end
 
# InputControl
class InputControl
  MOTION_MAGNITUDE = 0.01
 
  def initialize(player_object)
    @player_object = player_object
  end
 
  def update(dt)
    handle_motion_keys(dt)
  end
 
  private
 
    def handle_motion_keys(dt)
      h = (Gosu.button_down?(Gosu::KB_LEFT) ? -1 : 0) + (Gosu.button_down?(Gosu::KB_RIGHT) ? 1 : 0)
      v = (Gosu.button_down?(Gosu::KB_UP) ? -1 : 0) + (Gosu.button_down?(Gosu::KB_DOWN) ? 1 : 0)
      return unless h*h + v*v > 0
      @player_object.accelerate(dt*MOTION_MAGNITUDE, Math.atan2(v,h))
    end
end
 
# Joint
class Joint
  attr_accessor :velocity, :direction
 
  def initialize(x, y)
    @x_position = x # store the horizontal
    @y_position = y # and vertical positions
    @z_position = 0 # (not worried about z just yet)
 
    self.velocity = 0 # velocity stored in polar magnitude
    self.direction = 0 # and angle
 
    @image = Gosu::Image.new('joint.png') # just a png of a circle
    @font = Gosu::Font.new(20) # for debugging
  end
 
  def accelerate(magnitude, direction)
    delta_h_velocity = magnitude * Math.cos(direction) # acceleration applies a horizontal
    delta_v_velocity = magnitude * Math.sin(direction) # and vertical change in velocity
 
    h_velocity = self.velocity * Math.cos(self.direction) + delta_h_velocity
    v_velocity = self.velocity * Math.sin(self.direction) + delta_v_velocity
   
    self.velocity = Math.sqrt(h_velocity*h_velocity + v_velocity*v_velocity)
    self.direction = Math.atan2(v_velocity, h_velocity)
  end
 
  def halt
    self.velocity = 0 #stop
  end
 
  # apply velocity to the position
  def update(dt)
    h_velocity = dt * self.velocity * Math.cos(self.direction)
    v_velocity = dt * self.velocity * Math.sin(self.direction)
    @x_position += h_velocity
    @y_position += v_velocity
    halt if out_of_bounds?
  end
 
  def draw
    @image.draw(
      @x_position,
      @y_position,
      @z_position,
      0.15, 0.15
    )
 
    @font.draw_text("velocity: #{self.velocity}\ndirection: #{self.direction}", 0, 0, 0)
  end
 
  private
 
    def out_of_bounds?
      @x_position <= 0 || @x_position >= (640-20) || @y_position <= 0 || @y_position >= (480-20)
    end
end
 
tut = Tutorial.new
tut.show